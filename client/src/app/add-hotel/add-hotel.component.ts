import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-hotel',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.css']
})
export class AddHotelComponent implements OnInit {

  public addHotelForm = this.fb.group({
    name: ["", Validators.required],
  });

  constructor(public fb: FormBuilder, private http: Http,private router: Router) {}

  ngOnInit() {
  }

  addHotel(event){
    console.log(this.addHotelForm.value);
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    this.http.post('http://localhost/methotels/addHotel.php', JSON.stringify(this.addHotelForm.value), headers)
        .subscribe(
            (result) => {
              console.log(result);
              if(result.json().status === 200){
                alert("Success");
                this.router.navigate(['/'+'hotels']);
              } else {
                alert("error");
              }
              
          }, //For Success Response
            err => {console.error(err)} //For Error Response
        ); 
  }

}
