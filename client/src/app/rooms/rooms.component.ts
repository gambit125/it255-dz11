import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';


@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css'],
})
export class RoomsComponent implements OnInit {

  id: number;
  private sub: any;
  public rooms;

  constructor(private route: ActivatedRoute, private router: Router, private http: Http) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.id = +params['id']; // (+) converts string 'id' to a number

       this.http.get('http://localhost/methotels/rooms?id=' + this.id)
        .subscribe(
            (result) => {
              
              if(result.json().status === 200){
                this.rooms = result.json().data;
              } else {
                alert("There is no data");
              }
            }, //For Success Response
            err => {console.error(err)} //For Error Response
        );
    });
     
  }

  addRooms(){
    this.sub = this.route.params.subscribe(params => {
       this.router.navigate(['/'+'addroom/' , params['id']]);
    });
  }

}
