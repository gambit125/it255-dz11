import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByAreaAndNum'
})
export class FilterPipe implements PipeTransform {

  transform(rooms: any, term: any): any {
    if(term === undefined || term.length===0){
       return rooms
    } 

    let toRet = [];

    for(let i = 0 ; i < rooms.length ; i++){
      console.log(rooms[i]);
      if (parseInt(rooms[i].num) === parseInt(term) || parseInt(rooms[i].area) === parseInt(term)) {
        toRet.push(rooms[i]);
      }
        
    }

    return toRet;
  }

  

}
