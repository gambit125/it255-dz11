import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { router } from '../app.router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

  public hotels;

  constructor(private router: Router,private http: Http) { }

  ngOnInit() {
    this.http.get('http://localhost/methotels/allHotels.php')
        .subscribe(
            (data) => {
             this.hotels = data.json().data;
            }, //For Success Response
            err => {console.error(err)} //For Error Response
        ); 

  }

  seeRooms(id){
    console.log(id);
    console.log();
    router[0].redirectTo;
    this.router.navigate(['/'+'rooms/' + id]);
  }

  addHotels(){
    this.router.navigate(['/'+'addhotel']);
  }

}
