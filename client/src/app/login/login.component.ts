import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm = this.fb.group({
    username: ["", Validators.required],
    password: ["", Validators.required]
  });

  constructor(public fb: FormBuilder, private http: Http) {}

  ngOnInit() {
  }

  doLogin(event) {
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    this.http.post('http://localhost/methotels/login.php', JSON.stringify(this.loginForm.value), headers)
        .subscribe(
            () => {alert("Success")}, //For Success Response
            err => {console.error(err)} //For Error Response
        ); 
  }

}
