import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm = this.fb.group({
    username: ["", Validators.required],
    password: ["", Validators.required],
    email: ["", Validators.required],
    firstName: ["", Validators.required],
    lastName: ["", Validators.required],
  });

  constructor(public fb: FormBuilder, private http: Http) {}

  ngOnInit() {
  }

  doRegister(event){
    console.log(this.registerForm.value);
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    this.http.post('http://localhost/methotels/funkcije.php', JSON.stringify(this.registerForm.value), headers)
        .subscribe(
            () => {alert("Success")}, //For Success Response
            err => {console.error(err)} //For Error Response
        ); 
  }

}
