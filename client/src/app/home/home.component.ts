import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private myDatePickerOptions: IMyDpOptions = {
      // other options...
      dateFormat: 'dd.mm.yyyy',
  };

  private model: Object = { date: { year: 2018, month: 10, day: 9 } };

  constructor() { }

  ngOnInit() {
    // jQuerys("#owl-demo").owlCarousel({
    //     items : 1,
    //     lazyLoad : true,
    //     autoPlay : true,
    //     navigation : true,
    //     navigationText :  false,
    //     pagination : false,
    //   });
  }

}
