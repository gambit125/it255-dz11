import { ModuleWithProviders } from "@angular/core";
import { Routes , RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';  
import { HomeComponent } from './home/home.component';  
import { HotelsComponent } from './hotels/hotels.component';
import { RoomsComponent } from './rooms/rooms.component'; 
import { AddRoomComponent } from './add-room/add-room.component';
import { AddHotelComponent } from './add-hotel/add-hotel.component';


export const router: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'home', component: HomeComponent },
    { path: 'hotels', component: HotelsComponent },
    { path: 'rooms/:id', component: RoomsComponent },
    { path: 'addhotel', component: AddHotelComponent },
    { path: 'addroom/:id', component: AddRoomComponent }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);