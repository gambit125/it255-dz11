import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router , ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {

   public addRoomForm = this.fb.group({
    num: ["", Validators.required],
    area: ["", Validators.required],
    bedNum: ["", Validators.required],
  });

  id: number;
  private sub: any;

  constructor(public fb: FormBuilder, private activeRoute: ActivatedRoute, private http: Http,private router: Router) {} 

  ngOnInit() {
  }

  addRoom(event){
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    this.sub = this.activeRoute.params.subscribe(params => {
      let data = JSON.stringify(
          {
            num: parseInt(this.addRoomForm.value.num), 
            area: parseInt(this.addRoomForm.value.area) ,
            bedNum: parseInt(this.addRoomForm.value.bedNum),
            hotel_id: parseInt(params['id'])
          }
        )
      this.http.post('http://localhost/methotels/addRoom.php',data , headers)
        .subscribe(
            (result) => {
              console.log(result);
              if(result.json().status === 200){
                alert("Success");
                this.router.navigate(['/'+'rooms/' , params['id']]);
              } else {
                alert("error");
              }
              
          }, //For Success Response
            err => {console.error(err)} //For Error Response
        ); 
    });
    
  }

}
