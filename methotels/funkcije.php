<?php
	include("config.php");
	$jsonData = json_decode(file_get_contents('php://input'),TRUE);
	$ime = $jsonData['ime'];
	$prezime = $jsonData['prezime'];
	$email = $jsonData['email'];
	$username = $jsonData['username'];
	$password = $jsonData['password'];

	function registracija($ime, $prezime, $email, $username, $password) {
		global $conn;
		if (proveriPostojanje($username)) {
			echo null;
		} else {
			$insert = "INSERT INTO korisnici (ime, prezime, email, username, password) VALUES (?,?,?,?,?)";
			$query = $conn->prepare($insert);
			$query->bind_param('sssss',$ime,$prezime,$email,$username,md5($password));
			$query->execute();
			$query->close();
			$res["status"] = 200;
			$res["message"] = "Register Successfull";
			echo json_encode($res);
		}
	}

	function proveriPostojanje($username) {
		global $conn;
		$check = "SELECT * FROM korisnici WHERE username = ?";
		$query = $conn->prepare($check);
		$query->bind_param('s',$username);
		$query->execute();
		$query->store_result();
		if ($query->num_rows == 0) {
			return false;
		}
		$query->close();
		return true;
	}

	registracija($ime, $prezime, $email, $username, $password);
?>