<?php
	session_start();
	include("config.php");
	$jsonData = json_decode(file_get_contents('php://input'),TRUE);
	$username = $jsonData['username'];
	$password = $jsonData['password'];

	function logovanje($username, $password) {
		global $conn;
		$check = "SELECT * FROM korisnici WHERE username = ? AND password = ?";
		$query = $conn->prepare($check);
		$query->bind_param('ss',$username,md5($password));
		$query->execute();
		$query->store_result();
		if ($query->num_rows > 0) {
			return true;
		} else {
			return false;
		}
		$query->close();
	}

	if (!empty($username) && !empty($password)) {
		if (logovanje($username,$password)) {
			$_SESSION['username'] = $username;
			$res["status"] = 200;
			echo json_encode($res);
		} else {
			echo null
		}
	} else {
		echo null;
	}
?>