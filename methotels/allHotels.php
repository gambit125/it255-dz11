<?php
    header('Access-Control-Allow-Origin: *');
    include("config.php");

    function getAllHotels() {
        global $conn;
        $all = "SELECT * FROM hotels";
        $result = $conn->query($all);
		if ($result->num_rows > 0) {
            $res = array();
            $res["data"] = array();
			 while($row = $result->fetch_assoc()) {
                array_push($res["data"],$row); 
            }
            $res["status"] = 200;
            echo json_encode($res);
		} else {
            $res["status"] = 404;
            echo json_encode($res);
        }
    }

    getAllHotels();