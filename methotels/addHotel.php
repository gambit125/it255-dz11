<?php

    header('Access-Control-Allow-Origin: *');
    include("config.php");

    function addHotel() {
        global $conn;
        $jsonData = json_decode(file_get_contents('php://input'),TRUE);
        $add = "INSERT INTO hotels (name) VALUES (?)";
		$query = $conn->prepare($add);
        $query->bind_param('s',$jsonData['name']);
        $query->execute();
        $query->close();
        $res["status"] = 200;
        $res["message"] = $jsonData['name'];
        // print_r($_POST);
        // echo json_decode($entityBody);
        echo json_encode($res);
    }

    addHotel();