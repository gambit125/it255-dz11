<?php

    header('Access-Control-Allow-Origin: *');
    include("config.php");

    function addRoom() {
        global $conn;
        $jsonData = json_decode(file_get_contents('php://input'),TRUE);
        $add = "INSERT INTO rooms (num,area,bed_num,hotel_id) VALUES (?,?,?,?)";
		$query = $conn->prepare($add);
        $query->bind_param('dddd',$jsonData['num'],$jsonData['area'],$jsonData['bedNum'],$jsonData['hotel_id']);
        $query->execute();
        $query->close();
        $res["status"] = 200;
        $res["message"] = "Register Successfull";
        echo json_encode($res);
    }

    addRoom();