<?php

    header('Access-Control-Allow-Origin: *');
    include("config.php");

    function allRoomsByHotel() {
        global $conn;
        $query = "SELECT * FROM rooms JOIN hotels ON hotels.id = rooms.hotel_id WHERE hotel_id ="  . $_GET["id"];
        $result = $conn->query($query);
		if ($result->num_rows > 0) {
            $res = array();
            $res["data"] = array();
			 while($row = $result->fetch_assoc()) {
                array_push($res["data"],$row); 
            }
            $res["status"] = 200;
            echo json_encode($res);
		} else {
            $res["status"] = 404;
            echo json_encode($res);
        }

    }

    allRoomsByHotel();